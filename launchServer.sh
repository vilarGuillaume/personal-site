echo "Pour regarder tous les process lancés par hugo :"
echo 'netstat -p tcp -ano|grep "hugo"'
echo "Pour kill un process"
echo "kill -9 <PID>"

# sudo snap run hugo server --bind 0.0.0.0 -p 8080
# sudo hugo server -p 8080
sudo docker run -d -p 8080:8080 -v /root/personal-site:/hugo --rm hugo 
