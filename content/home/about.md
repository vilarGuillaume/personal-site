+++
# About/Biography widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 5


# List your academic interests.
[interests]
  interests = [
    "Network",
    "Production",
    "Development",
    "Security",
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "Engineer degree (equivalent to Master degree) in computer science specialized in telecommunication and networks"
  institution = "EPITA, Paris, France"
  year = 2019

[[education.courses]]
  course = "Preparatory classes: Two-year highly selective classes to prepare for engineering schools. Specialised in physics & mathematics (PCSI/PSI)" 
  institution = "Lycée Bellevue, Toulouse, France"
  year = 2016

+++



# Biography

Lena Smith is a professor of artificial intelligence at the Stanford AI Lab. Her research interests include distributed robotics, mobile computing and programmable matter. She leads the Robotic Neurobiology group, which develops self-reconfiguring robots, systems of self-organizing robots, and mobile sensor networks.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed neque elit, tristique placerat feugiat ac, facilisis vitae arcu. Proin eget egestas augue. Praesent ut sem nec arcu pellentesque aliquet. Duis dapibus diam vel metus tempus vulputate. 
