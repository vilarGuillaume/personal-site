+++
# Accomplishments widget.
widget = "accomplishments"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Certifications"
subtitle = ""

# Order that this section will appear in.
weight = 15

# Date format
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Accomplishments.
#   Add/remove as many `[[item]]` blocks below as you like.
#   `title`, `organization` and `date_start` are the required parameters.
#   Leave other parameters empty if not required.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

[[item]]
  title = "CCNA R&S : Cisco Certified Network Associate - Routing and Switching"
  organization = "Cisco"
  certificate_url = "https://www.cisco.com/c/en/us/training-events/training-certifications/certifications/associate/ccna-routing-switching.html"
  date_start = "2018-09-01"
  date_end = ""
  description = "One of the most recognised certifications in the industry: validates the ability to install, configure, operate, and troubleshoot medium-size route and switched networks, including implementation and verification of connections to remote sites in a WAN. CCNA curriculum includes basic mitigation of security threats, introduction to wireless networking concepts and terminology, and performance-based skills. This new curriculum also includes (but is not limited to) the use of these protocols: IP, Enhanced Interior Gateway Routing Protocol (EIGRP), Serial Line Interface Protocol Frame Relay, Routing Information Protocol Version 2 (RIPv2),VLANs, Ethernet, access control lists (ACLs).<br/><br/>Exam: ICND2 (Interconnecting Cisco Networking Devices 2)"

[[item]]
  title = "CCENT: Cisco Certified Entry Networking Technician"
  organization = "Cisco"
  certificate_url = "https://www.cisco.com/c/en/us/training-events/training-certifications/certifications/entry/ccent.html"
  date_start = "2018-05-01"
  date_end = ""
  description = "Cisco Certified Entry Networking Technician (CCENT) validates the ability to install, operate and troubleshoot a small enterprise branch network, including basic network security. With a CCENT, a network professional demonstrates the skills required for entry-level network support positions - the starting point for many successful careers in networking. The curriculum covers networking fundamentals, WAN technologies, basic security and wireless concepts, routing and switching fundamentals, and configuring simple networks. CCENT is the first step toward achieving CCNA, which covers medium-size enterprise branch networks with more complex connections.<br/><br/>Exam passed: ICND1 (Interconnecting Cisco Networking Devices 1)"
  
+++
