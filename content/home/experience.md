+++
# Experience widget.
widget = "experience"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Professional Experience"
subtitle = ""

# Order that this section will appear in.
weight = 8

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "January 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.

# [[experience]]
#  title = "CEO"
#  company = "GenCoin"
#  company_url = ""
#  location = "Limerick, Ireland"
#  date_start = "2019-01-01"
#  date_end = "2019-08-31"
#  description = """
#  Responsibilities include:
  
#  * Analysing
#  * Modelling
#  * Deploying
#  """

[[experience]]
  title = "Web Developer"
  company = "CNP TI"
  company_url = ""
  location = "Paris, France"
  date_start = "2017-09-01"
  date_end = "2017-12-31"
  description = """
CNPTI manages the IT of CNP insurance, a leader of the loan insurance in France. In a team of 3 developers, we were in charge of developing a full stack
website to access to all the applications of the company. This mission included:

  * Agile/SCRUM methodologies
  * Java 7 with Spring technologies
  * Jenkins and Ansible for DevOps methodology
  * Oracle Database
  * Test Driven Development
  """

+++
