# [Personal Site](http://212.47.253.179:8080)
In order to create my personal site, I used the framework [Hugo](https://gohugo.io/) with the theme Academic.
I strongly recommend the framework hugo, because it help to you create a beautiful website quickly. Perfect for personal, student, or academic websites.

I've decided to put my application into a Docker container to make it easier to reuse and to migrate.
Basically, you just need Docker to run the application, and to modify it.

[![Screenshot](https://raw.githubusercontent.com/gcushen/hugo-academic/master/academic.png)](https://github.com/gcushen/hugo-academic/)

## Getting started

Prerequisite:

* [Install Docker](https://docs.docker.com/install/)

1. Clone the repositorys with Git: 

       git clone https://gitlab.com/vilarGuillaume/personal-site My_Website
    
2. Build the image:

       cd My_Website
       sudo docker build . --tag hugo

3. Run the container with the script:
      
       ./launchServer.sh

    Now you can go to [localhost:8080](http://localhost:8080) and your website should appear.
  
4. Read the [Quick Start Guide](https://sourcethemes.com/academic/docs/) to learn how to add Markdown content, customize your site, and deploy it. You can also read the [Hugo documentation](https://gohugo.io/documentation/)

## License

Copyright 2017 [George Cushen](https://georgecushen.com).

Released under the [MIT](https://github.com/sourcethemes/academic-kickstart/blob/master/LICENSE.md) license.

[![Analytics](https://ga-beacon.appspot.com/UA-78646709-2/academic-kickstart/readme?pixel)](https://github.com/igrigorik/ga-beacon)
