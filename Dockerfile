FROM golang 
WORKDIR /opt/hugo

# ADD . /opt/hugo/

ENV HUGO_VERSION 0.53
ENV HUGO_BIN_NAME hugo_${HUGO_VERSION}_Linux-64bit.tar.gz
ENV HUGO_BIN_URL https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_BIN_NAME}
RUN wget -qO- "${HUGO_BIN_URL}" | tar xz
ENV PATH "/opt/hugo:${PATH}"
WORKDIR /hugo
# CMD ["hugo", "version"]
EXPOSE 1313
CMD ["hugo", "server", "--bind", "0.0.0.0", "-p", "8080"]
